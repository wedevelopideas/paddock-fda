<?php

return [
    'email' => 'Email',
    'login' => 'Login',
    'logout' => 'Logout',
    'password' => 'Password',
];
