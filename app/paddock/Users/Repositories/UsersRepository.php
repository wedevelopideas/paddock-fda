<?php

namespace App\paddock\Users\Repositories;

use App\paddock\Users\Models\Users;

class UsersRepository
{
    /**
     * @var Users
     */
    private $users;

    /**
     * UsersRepository constructor.
     * @param  Users  $users
     */
    public function __construct(Users $users)
    {
        $this->users = $users;
    }
}
